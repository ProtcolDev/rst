-- GLOBAL DEFINITIONS --[[
PROGRAM_NAME = "NS v4.0"
PROTOCOL_VERSION = 4.0

MAX_RAMP_RATES =						    {HEATING = 12, COOLING = 8}
SLOW_FILL_MAX_PRESSURE = 					36
SLOW_FILL_PERIOD_MS = 						125
SLOW_FILL_ACTUATION_TIME_MS = 				3
MAX_TEMPERATURE = 							105
MIN_TEMPERATURE = 							40
PISTON_FILL_RATE = 							1000

POST_FILL_DELAY_MS = 					    250
POST_HARDSEAL_CLOSE_DELAY_MS = 				50
POST_PLUNGE_DELAY_MS = 						250

SEAL_TIME_SECONDS = 						13

PCR_I_TEMPERATURE_QUERYABLE =               "PCR1Temperature_C"
PCR_II_TEMPERATURE_QUERYABLE =              "PCR2Temperature_C"
FLUOR_TEMP_CAPTURE_QUERYABLE =              "FluorTempCapture"
IMAGE_CAPTURE_QUERYABLE =                   "LastImageCapture"
SCRIPT_OUTPUT_QUERYABLE =                   "ScriptOutput"
LAST_FUNCTION_CALL_QUERYABLE =              "LastInstrumentFunctionCall"

BEAD_BEAT_MAGBEAD_HANDLING =				{
                                                BEAD_BEAT_SPLIT_MAGBEADS =		true,
                                                BEAD_BEAT_WITH_MAGBEADS =		false,
                                                BEAD_BEAT_AND_MIX_MAGBEADS =	false
                                            }

BEAD_BEAT_SECONDS =							20
BEAD_BEAT_RPM =								12000

PRE_PLUNGE_ELUTION =						{PRE_PLUNGE = true, PLUNGED = false}
PRE_HEAT_ELUTION =							true
PRE_HEAT_ELUTION_TEMPERATURE =			    85
PRE_HEAT_ELUTION_RAMP_RATE =				MAX_RAMP_RATES
PRE_PLUNGE_DILUTION =						{PRE_PLUNGE = true, PLUNGED = false}

COLLECTION_STAGES =							{{
                                                CYCLES = 					3,
                                                SETTLE_DELAY_SECONDS =  	1,
                                                SLOW_FILL_PRESSURES = 	{
                                                                                A = 16,
                                                                                B = 0,
                                                                                C = 29
                                                                            }
                                            },{
                                                CYCLES = 					1,
                                                SETTLE_DELAY_SECONDS = 	0,
                                                SLOW_FILL_PRESSURES = 	{
                                                                                A = 16,
                                                                                B = 24,
                                                                                C = 29
                                                                            }
                                            }}

WASH_COUNT =								1

USE_HEATED_ELUTION =							true
ELUTION_TEMPERATURE =						95
ELUTION_RAMP_RATE =							MAX_RAMP_RATES
ELUTION_HOLD_SECONDS =						0

MASTER_MIX_I_HOT_START_TEMPERATURE = 	54
MASTER_MIX_I_HOT_START_RAMP_RATE =   	MAX_RAMP_RATES
MASTER_MIX_I_HOT_START_HOLD_SECONDS = 	2

REVERSE_TRANSCRIPTION_TEMPERATURE =     54
REVERSE_TRANSCRIPTION_RAMP_RATE =       MAX_RAMP_RATES
REVERSE_TRANSCRIPTION_HOLD_SECONDS =    15

PCR_I_STAGES =  {
{
CYCLES =                    1,
DENATURE_TEMPERATURE =      99,
RAMP_RATES =                {HEATING= 12, COOLING = 8},
DENATURE_HOLD_SECONDS =     10,
ANNEALING_TEMPERATURE =     55,
ANNEALING_HOLD_SECONDS =    3,

SQUISH_HALF_TO_WASTE = false
},
{
CYCLES =                    5,
DENATURE_TEMPERATURE =      101,
RAMP_RATES =                {HEATING= 12, COOLING = 8},
DENATURE_HOLD_SECONDS =     7,
ANNEALING_TEMPERATURE =     55,
ANNEALING_HOLD_SECONDS =    3,

SQUISH_HALF_TO_WASTE = true
},
{
CYCLES =                    19,
DENATURE_TEMPERATURE =      101,
RAMP_RATES =                {HEATING= 12, COOLING = 8},
DENATURE_HOLD_SECONDS =     2,
ANNEALING_TEMPERATURE =     55,
ANNEALING_HOLD_SECONDS =    1,

SQUISH_HALF_TO_WASTE = false
},
}

PCR_MM_II_HOT_START_TEMPERATURE =			98
ARRAY_FLOOD_TEMPERATURE =					80
ARRAY_FLOOD_DELAY_SECONDS = 				5

TEMP_STEPPING = {
	{
		PCR_1 = PCR_MM_II_HOT_START_TEMPERATURE,
		PCR_2 = ARRAY_FLOOD_TEMPERATURE,
		PCR_2_DELAY_SECONDS = 2
	},
	{
		PCR_1 = PCR_MM_II_HOT_START_TEMPERATURE,
		PCR_2 = ARRAY_FLOOD_TEMPERATURE,
		PCR_2_DELAY_SECONDS = 2
	},
}

PRE_MELT_RAMP_RATE =                        {HEATING = 2}
PRE_MELT_HOLD_SECONDS =                     5

PCR_II_STAGES = {
{
CYCLES =                    27,
DENATURE_TEMPERATURE =      98,
RAMP_RATES =                {HEATING = 12, COOLING = 7},
DENATURE_HOLD_SECONDS =     2,
ANNEALING_TEMPERATURE =     59,
ANNEALING_HOLD_SECONDS =    1,

MELT =  {
    START_TEMPERATURE = 68,
    RAMP_TEMPERATURE =  101,
    RAMP_RATES =         {HEATING = 2}
}
},
}

--]]

-- PROTOCOL STEPS --[[
function initialize()
	set_protocol_data_label("Initialize")
	
	start_collecting_protocol_data_periodically(PCR_I_TEMPERATURE_QUERYABLE, 0.5)	
	start_collecting_protocol_data_subscription( IMAGE_CAPTURE_QUERYABLE )
	start_collecting_protocol_data_subscription( SCRIPT_OUTPUT_QUERYABLE )
	start_collecting_protocol_data_subscription( LAST_FUNCTION_CALL_QUERYABLE )

	if  SEAL_TIME_SECONDS < 10 or SEAL_TIME_SECONDS > 20 then
		create_error( "Pouch seal time value " .. SEAL_TIME_SECONDS .. " out of protocol's defined range of " .. 10 .. " to " .. 20 .. ".")
	end
	
	seal_pouch_no_wait(SEAL_TIME_SECONDS)
	start_reservoir_pressure_regulation_no_wait()
	
	prerun_system_init()	
	
	wait_for_reservoir_pressure_regulation()
	pressurize_manifold( )	

	slow_fill_or_fill_bladder( BLISTERS.ARRAY )
	activate_excitation_led()
	find_mask_no_wait()
	
	close_hardseals( "F_FITMENT", "A_FITMENT", "B_FITMENT", "D_FITMENT", "F_G" )
	close_path(BLISTERS.MASTER_MIX_II, BLISTERS.LYSIS_BEADS)
	slow_fill_or_fill_bladder( BLISTERS.LYSIS_BEADS)
		
	wait_for_seal_pouch()	
	check_for_used_pouch()
	wait_for_find_mask()
	capture_image_with_mask( "Image with mask" )
	deactivate_excitation_led()	
	bladder_vent(BLISTERS.ARRAY)
end

function extract_nucleic_acids()   
	set_protocol_data_label("Extracting Nucleic Acids")	
	assert_sample_prep_global_constants()

	local settle_time_seconds = 25

	bead_beat() 
	settle_lysis_beads( settle_time_seconds )	
end

function capture_nucleic_acids()
	set_protocol_data_label("Capturing Nucleic Acids")

	local capture_beads_table =	{
									pre_magnet_delay_seconds =	0,
									post_magnet_delay_seconds =	2,
									cycle_magnet =					false,
									use_magnet =					true
								}
	open_path(BLISTERS.LYSIS_BEADS, BLISTERS.MAGNET)

	for i=1,#COLLECTION_STAGES do
		local collection_stage = COLLECTION_STAGES[i]
		collect_magbeads( collection_stage, capture_beads_table )
	end
	
	close_path(BLISTERS.BEAD_MIX, BLISTERS.LYSIS_BEADS)
	if capture_beads_table.use_magnet then
		magnet_retract()
	end
end

function wash_nucleic_acids()
	set_protocol_data_label("Washing Nucleic Acids")
	local capture_beads_table =	{
									pre_magnet_delay_seconds =	0,
									post_magnet_delay_seconds =	2,
									cycle_magnet =					true,
									use_magnet =					true
								}
	local slow_fill_table =			{
									B =	29,
									C =	29
								}
	local mix_cycles = 			0

	for i=1, WASH_COUNT do
		local wash_fitment_location = FITMENT["WASH_BUFFER_" .. i]
		if wash_fitment_location == nil then
			create_error( "WASH_COUNT " .. WASH_COUNT .. " not defined in FITMENT layout.")
		end
		move_liquid(wash_fitment_location,  BLISTERS.BEAD_MIX, slow_fill_table)
		open_path(BLISTERS.BEAD_MIX, BLISTERS.MAGNET)
		slow_fill_or_fill_bladder( BLISTERS.BEAD_MIX, slow_fill_table )
		mix_liquid(BLISTERS.MAGNET, BLISTERS.BEAD_MIX, mix_cycles, slow_fill_table)
		capture_beads( capture_beads_table )
		move_liquid(BLISTERS.MAGNET, BLISTERS.WASTE, slow_fill_table)
		if capture_beads_table.cycle_magnet and capture_beads_table.use_magnet then
			magnet_retract()
		end
	end
	if capture_beads_table.use_magnet then
		magnet_retract() 
	end
end

function elute_nucleic_acids()
	set_protocol_data_label("Eluting Nucleic Acids")
	local elution_mix_count = 	3
	local use_three_blister_elution =	false
	local capture_beads_table =	{
									pre_magnet_delay_seconds = 0,
									post_magnet_delay_seconds = 1,
									use_magnet = true,
									cycle_magnet = true
								}
	local slow_fill_table =		{
									C = 32
								}
    
    perform_elution( elution_mix_count, use_three_blister_elution, capture_beads_table, slow_fill_table )  
end   

function prepare_PCR_I()
	set_protocol_data_label("PCR1 Mechanical Hot Start")
	assert_pcr_global_constants()

	move_liquid( BLISTERS.ELUTION, BLISTERS.PRIMER_PILL )
	ramp_to_temp( PELTIERS.PCR1, MASTER_MIX_I_HOT_START_TEMPERATURE, MASTER_MIX_I_HOT_START_RAMP_RATE )
	move_liquid(FITMENT.MASTER_MIX_I, BLISTERS.PCR_I_PELTIER1)
	fill_path(BLISTERS.PCR_I_PELTIER1, BLISTERS.PRIMER_PILL)	
	seconds_delay(MASTER_MIX_I_HOT_START_HOLD_SECONDS)
	move_liquid(BLISTERS.PRIMER_PILL, BLISTERS.PCR_I_PELTIER2)
	mix_liquid(BLISTERS.PCR_I_PELTIER1, BLISTERS.PCR_I_PELTIER2, 10)
end

function perform_reverse_transcription()       
	set_protocol_data_label("Performing Reverse Transcription") 
	local mix_seconds =				2 
	local rest_seconds =				18

	ramp_to_temp( PELTIERS.PCR1, REVERSE_TRANSCRIPTION_TEMPERATURE, REVERSE_TRANSCRIPTION_RAMP_RATE )

	local start_time = elapsed_time()
	while get_time_elapsed(start_time) < REVERSE_TRANSCRIPTION_HOLD_SECONDS do		
		bladder_vent(BLISTERS.PCR_I_PELTIER1)		
		slow_fill_or_fill_bladder(BLISTERS.PCR_I_PELTIER2)
		seconds_delay( mix_seconds / 2 )
		bladder_vent(BLISTERS.PCR_I_PELTIER2)
		slow_fill_or_fill_bladder(BLISTERS.PCR_I_PELTIER1)
		seconds_delay( mix_seconds / 4 )
		slow_fill_or_fill_bladder(BLISTERS.PCR_I_PELTIER2)
		seconds_delay( mix_seconds / 4 )
		
		local time_elapsed = get_time_elapsed(start_time)

		if time_elapsed > REVERSE_TRANSCRIPTION_HOLD_SECONDS then 	
			return
		end

		if time_elapsed + rest_seconds > REVERSE_TRANSCRIPTION_HOLD_SECONDS then
			seconds_delay(REVERSE_TRANSCRIPTION_HOLD_SECONDS - time_elapsed)
		else			
			seconds_delay(rest_seconds)
		end
	end
end

function perform_PCR_I()	
	for i=1,#PCR_I_STAGES do
		set_protocol_data_label("PCR1 " .. i)
		perform_PCR_I_stage(i)
		
		if PCR_I_STAGES[i].SQUISH_HALF_TO_WASTE then
			split_liquid( BLISTERS.PCR_I_PELTIER2, BLISTERS.PCR_I_PELTIER1 )
			open_path( BLISTERS.PCR_I_PELTIER1, BLISTERS.WASTE )
			close_path( BLISTERS.PCR_I_PELTIER1, BLISTERS.MAGNET )
			fill_path( BLISTERS.PCR_I_PELTIER1, BLISTERS.BEAD_MIX )
			unseal_path( BLISTERS.PCR_I_PELTIER2, BLISTERS.PCR_I_PELTIER1 )
		end		
	end
	fill_path( BLISTERS.PCR_I_PELTIER2, BLISTERS.PCR_I_PELTIER1 )
end

function prepare_PCR_II()
	set_protocol_data_label("Dilution")
	
	ramp_to_temp( PELTIERS.PCR1, PCR_MM_II_HOT_START_TEMPERATURE, MAX_RAMP_RATES )
		
	perform_PCR_I_dilution()
	
	add_master_mix_II()	
		
	set_protocol_data_label("Flood PCR II Array")   
	slow_fill_or_fill_bladder( BLISTERS.ARRAY )	
	
	for i = 1, #TEMP_STEPPING do		
		set_protocol_data_label("pre flood temp step " .. i) 
		ramp_to_temp( PELTIERS.PCR1, TEMP_STEPPING[i].PCR_1, MAX_RAMP_RATES)
		stop_collecting_protocol_data( PCR_I_TEMPERATURE_QUERYABLE )	
		temp_control_off()	
		start_collecting_protocol_data_periodically(PCR_II_TEMPERATURE_QUERYABLE, 0.5)
		ramp_to_temp( PELTIERS.PCR2, TEMP_STEPPING[i].PCR_2, MAX_RAMP_RATES)
		seconds_delay( TEMP_STEPPING[i].PCR_2_DELAY_SECONDS )
		if i ~= #TEMP_STEPPING then
			stop_collecting_protocol_data( PCR_II_TEMPERATURE_QUERYABLE )	
			temp_control_off()	
			start_collecting_protocol_data_periodically(PCR_I_TEMPERATURE_QUERYABLE, 0.5)
		end
	end

	flood_array()

end

function perform_PCR_II_Stage_1()     
	set_protocol_data_label("PCR2 Stage 1")
	start_collecting_protocol_data_subscription(FLUOR_TEMP_CAPTURE_QUERYABLE)
	perform_PCR_II_stage( 1 )
	stop_collecting_protocol_data(FLUOR_TEMP_CAPTURE_QUERYABLE)
end

function perform_melt_1()
    set_protocol_data_label("POSTHEAT 1")
    ramp_to_temp( PELTIERS.PCR2, 98, MAX_RAMP_RATES )
    seconds_delay(2)
    ramp_to_temp( PELTIERS.PCR2, 59, MAX_RAMP_RATES )
    seconds_delay( 1 )

    set_protocol_data_label("MELT 1")
    local pcr2_stage_index = 1
    perform_melt_from_pcr2_stage( pcr2_stage_index )
end

function finishing()
	plunge_piston(FITMENT.WELL_2) 
	plunge_piston(FITMENT.WASH_BUFFER_2)
	stop_collecting_protocol_data( PCR_II_TEMPERATURE_QUERYABLE )
	stop_collecting_protocol_data( IMAGE_CAPTURE_QUERYABLE )
	temp_control_off()
	stop_reservoir_pressure_regulation()
end

STEPS =	{
{ NAME = "Initialize", FUNCTION = initialize },
{ NAME = "Extracting Nucleic Acids", FUNCTION = extract_nucleic_acids },
{ NAME = "Capturing Nucleic Acids", FUNCTION = capture_nucleic_acids },
{ NAME = "Washing Nucleic Acids", FUNCTION = wash_nucleic_acids },
{ NAME = "Eluting Nucleic Acids", FUNCTION = elute_nucleic_acids },
{ NAME = "Preparing PCR I", FUNCTION = prepare_PCR_I },
{ NAME = "Performing Reverse Transcription", FUNCTION = perform_reverse_transcription },
{ NAME = "Performing PCR I", FUNCTION = perform_PCR_I },
{ NAME = "Preparing PCR II", FUNCTION = prepare_PCR_II },
{ NAME = "Performing PCR II Stage 1", FUNCTION = perform_PCR_II_Stage_1 },
{ NAME = "Performing Melt 1", FUNCTION = perform_melt_1 },
{ NAME = "Finishing", FUNCTION = finishing }
}
--]]

-- GENERAL --[[
function assert_boolean_value( name, value )
	if type(value) == "table" then
		for key,v in pairs(value) do	
			assert_boolean_value(key, v)
		end
	elseif type(value) ~= "boolean" then
			create_error( name .. " must be boolean value.")
	end
end

function get_time_elapsed(start_time)
	return elapsed_time() - start_time
end

function create_error(error)
		return terminate_protocol_with_error("Protocol defined error: " .. error)
end

function check_temperature_limits( temperature )
	if  temperature < MIN_TEMPERATURE or temperature > MAX_TEMPERATURE then
		create_error( "Temperature value " .. temperature .. " out of protocol's defined range of " .. MIN_TEMPERATURE .. " to " .. MAX_TEMPERATURE .. ".")
	end
end

function check_ramp_rate_limits(is_heating, peltier, ramp_rates )
	local max_heating = MAX_RAMP_RATES.HEATING
	local max_cooling = MAX_RAMP_RATES.COOLING
	local heating_ramp_rate = ramp_rates.HEATING
	local cooling_ramp_rate = ramp_rates.COOLING
	
	if is_heating and heating_ramp_rate == nil then
		create_error("Attempted to heat " .. peltier .. " peltier without defining a heating ramp rate")
		
	elseif not is_heating and cooling_ramp_rate == nil then
		create_error("Attempted to cool " .. peltier .. " peltier without defining a cooling ramp rate")
	end
 		
	if is_heating and (heating_ramp_rate < 0.1 or heating_ramp_rate > max_heating) then
		create_error( "For " .. peltier .. " peltier, ramp rate value " .. ramp_rates.HEATING .. " is not within protocol's defined heating range of " .. 0.1 .. " to " .. max_heating.. ".")
	elseif not is_heating and (cooling_ramp_rate < 0.1 or cooling_ramp_rate > max_cooling) then
		create_error( "For " .. peltier .. " peltier, ramp rate value " .. ramp_rates.COOLING .. " is not within protocol's defined cooling range of " .. 0.1 .. " to " .. max_cooling.. ".")
	end
end

function get_ramp_rate( is_heating, ramp_rates )
	if is_heating then
		return ramp_rates.HEATING
	else
		return ramp_rates.COOLING
	end
end

function is_heating(current_temperature, target_temperature)
	if target_temperature > current_temperature then
		return true
	else
		return false
	end
end

function get_peltier_temperature(peltier)
	if peltier == PELTIERS.PCR1 then
		return get_queryable(PCR_I_TEMPERATURE_QUERYABLE)
	else
		return get_queryable(PCR_II_TEMPERATURE_QUERYABLE) 
	end
end

function ramp_to_temp( peltier, target_temperature, ramp_rates )
    check_temperature_limits( target_temperature )	
    local current_temperature = get_peltier_temperature(peltier)
    local is_heating = is_heating(current_temperature, target_temperature)
    check_ramp_rate_limits( is_heating, peltier, ramp_rates )
    local ramp_rate = get_ramp_rate(is_heating, ramp_rates)
       
   	if peltier == PELTIERS.PCR1 then
		ramp_to_temp_pcr1(target_temperature, ramp_rate)
	elseif peltier == PELTIERS.PCR2 then
		ramp_to_temp_pcr2(target_temperature, ramp_rate)
	end
end

function ramp_to_temp_no_wait( peltier, target_temperature, ramp_rates )
    check_temperature_limits( target_temperature )	
    local current_temperature = get_peltier_temperature(peltier)
    local is_heating = is_heating(current_temperature, target_temperature)
    check_ramp_rate_limits( is_heating, peltier, ramp_rates)
    local ramp_rate = get_ramp_rate(is_heating, ramp_rates)
    
	if peltier == PELTIERS.PCR1 then
		ramp_to_temp_pcr1_no_wait(target_temperature, ramp_rate)
	elseif peltier == PELTIERS.PCR2 then
		ramp_to_temp_pcr2_no_wait(target_temperature, ramp_rate)
	end
end

function get_table_index( table, value )
	for i,v in ipairs(table) do
		if v == value then
			return i
		end
	end
end

function get_piston_bundle( piston )
	if piston == 4 then
		return { 3, 4 }
	elseif piston == 5 then
		return { 3, 4, 5 }
	elseif piston == 6 then
		return { 1, 6 }
	elseif piston == 7 then
		return { 1, 6, 7}
	elseif piston == 9 then
		return { 8, 9 }
	elseif piston == 10 then
		return { 8, 9, 10 }
	elseif piston == 11 then
		create_error( "Pouch does not support plunging of well 11.")
	else 
		return { piston }
	end
end

function plunge_piston( fitment_location )	
	if PISTON_FILL_RATE < 500 then
		create_error("PISTON_FILL_RATE must be greater than 500 milliseconds")
	end
	
	bladder_vent( fitment_location.CHANNEL_BLISTER )
	hardseal_open( fitment_location.HARDSEAL )

	local bundle = get_piston_bundle( fitment_location.WELL )
	local plunge_pressure = 55.0
	piston_extend_with_delay( plunge_pressure, PISTON_FILL_RATE, POST_PLUNGE_DELAY_MS, table.unpack( bundle ))

	hardseal_close_with_delay( fitment_location.HARDSEAL )
end

function hardseal_close_with_delay( hardseal )
	if POST_HARDSEAL_CLOSE_DELAY_MS == nil then
		create_error("A post hardseal close delay must be set.")
	end
	hardseal_close( hardseal, POST_HARDSEAL_CLOSE_DELAY_MS )	
end

function slow_fill_or_fill_bladder( bladder, slow_fill_table )
	if slow_fill_table ~= nil then 
		for key,value in pairs(slow_fill_table) do 
			if value > SLOW_FILL_MAX_PRESSURE then
				create_error("Bladder" .. key .. " puff value must not exceed " .. SLOW_FILL_MAX_PRESSURE .. " PSI")
			end 
		end 
	end
	if SLOW_FILL_ACTUATION_TIME_MS > SLOW_FILL_PERIOD_MS then
		create_error("SLOW_FILL_ACTUATION_TIME_MS must be less than " .. SLOW_FILL_PERIOD_MS)
	end
	local slow_fill = slow_fill_table ~= nil and get_table_index(SLOW_FILL_BLADDERS, bladder) ~= nil and slow_fill_table[bladder] ~= nil and slow_fill_table[bladder] ~= 0
	if slow_fill then
		bladder_slow_fill_to_pressure(bladder, SLOW_FILL_ACTUATION_TIME_MS, SLOW_FILL_PERIOD_MS, slow_fill_table[bladder]) 		
	else		
		assert(POST_FILL_DELAY_MS) 
		bladder_fill(bladder, POST_FILL_DELAY_MS)
	end
end

function get_movement_iterator( start_index, end_index )
	if( start_index > end_index )then
		return  -1
	else
		return  1
	end
end

function get_hardseal(bladder, direction)
	if direction == 1 then
		return bladder .. "_" .. BLADDERS[get_table_index(BLADDERS, bladder) + 1]
	elseif direction == -1 then
		return BLADDERS[get_table_index(BLADDERS, bladder) - 1] .. "_" .. bladder
	else
		--do nothing
	end
end

function move_liquid( start_location, end_location, slow_fill_table ) --Plunges fitment well if needed then moves liquid to desired end location, uses slow fill for bladders defined in table.
	if start_location["WELL"] ~= nil then
		plunge_piston( start_location )
		start_location = start_location.CHANNEL_BLISTER
	end
	open_path(start_location, end_location)
	close_path(start_location, end_location, slow_fill_table)
end

function mix_liquid( start_bladder, end_bladder, mixCount, slow_fill_table) --opens path and mixes between two bladders, uses slow fill for bladders defined in table.
	if mixCount == 0 then
		return
	end

	local start_index = get_table_index( BLADDERS, start_bladder )	
	local end_index = get_table_index( BLADDERS, end_bladder )
	local iterator = get_movement_iterator(start_index, end_index )        
	open_path(start_bladder, end_bladder)
	for i=1,mixCount do
		local stop_bladder = BLADDERS[end_index - iterator]
		fill_path( start_bladder, stop_bladder, slow_fill_table)
		vent_path( start_bladder, stop_bladder )
		stop_bladder = BLADDERS[start_index + iterator]
		fill_path( end_bladder, stop_bladder, slow_fill_table )
		vent_path( end_bladder, stop_bladder )
	end
end

function split_liquid( start_location, end_location) --inflates bladders and seals path to divide liquid equally, note: does not first unseal the path
	fill_path(start_location, end_location)
	seconds_delay(0.5)
	seal_path(start_location, end_location)
end

function open_path(start_bladder, end_bladder) --vents bladders and opens hardseals sequentially. note: does not vent start_bladder
	local start_index = get_table_index( BLADDERS, start_bladder )
	local end_index = get_table_index( BLADDERS, end_bladder )
	local iterator = get_movement_iterator(start_index, end_index)

	for current_bladder_index = start_index, end_index -iterator, iterator do
		local current_bladder = BLADDERS[current_bladder_index]
		local next_bladder = BLADDERS[current_bladder_index + iterator]
		local joining_hardseal = get_hardseal(current_bladder, iterator)
		bladder_vent(next_bladder)
		hardseal_open(joining_hardseal)
	end    
end

function close_path(start_bladder, end_bladder, slow_fill_table)  --fill bladders and close hardseals sequentially, uses slow fill for bladders defined in table. note: does not fill end_bladder
	local start_index = get_table_index( BLADDERS, start_bladder )
	local end_index = get_table_index( BLADDERS, end_bladder )
	local iterator = get_movement_iterator(start_index, end_index)

	for current_bladder_index = start_index, end_index -iterator, iterator do
		local current_bladder = BLADDERS[current_bladder_index]
		local joining_hardseal = get_hardseal(current_bladder, iterator)
		slow_fill_or_fill_bladder( current_bladder, slow_fill_table)

		hardseal_close_with_delay(joining_hardseal)
	end
end

function vent_path(start_bladder, end_bladder) 	--vent all bladders between and including the two bladders named
	local start_index = get_table_index( BLADDERS, start_bladder )
	local end_index = get_table_index( BLADDERS, end_bladder )
	local iterator = get_movement_iterator(start_index, end_index)

	for current_bladder_index = start_index, end_index, iterator do
		bladder_vent(BLADDERS[current_bladder_index])
	end
end

function fill_path(start_bladder, end_bladder, slow_fill_table )	--fill all bladders between and including the two bladders named, uses slow fill for bladders defined in table
	local start_index = get_table_index( BLADDERS, start_bladder )
	local end_index = get_table_index( BLADDERS, end_bladder )
	local iterator = get_movement_iterator(start_index, end_index)

	for current_bladder_index = start_index, end_index, iterator do
		local current_bladder = BLADDERS[current_bladder_index]
		slow_fill_or_fill_bladder( current_bladder, slow_fill_table)
	end
end

function unseal_path(start_bladder, end_bladder)	-- open all hardseals between two bladders
	local start_index = get_table_index( BLADDERS, start_bladder )
	local end_index = get_table_index( BLADDERS, end_bladder )
	local iterator = get_movement_iterator(start_index, end_index)

	for current_bladder_index = start_index, end_index - iterator, iterator do
		local current_bladder = BLADDERS[current_bladder_index]
		hardseal_open(get_hardseal(current_bladder, iterator))
	end
end

function seal_path(start_bladder, end_bladder)	-- close all hardseals between two bladders
	local start_index = get_table_index( BLADDERS, start_bladder )
	local end_index = get_table_index( BLADDERS, end_bladder )
	local iterator = get_movement_iterator(start_index, end_index)

	for current_bladder_index = start_index, end_index - iterator, iterator do
		local current_bladder = BLADDERS[current_bladder_index]
		hardseal_close_with_delay(get_hardseal(current_bladder, iterator))
	end
end

function close_hardseals( ... )
	for index,hardseal in ipairs( table.pack(...) ) do
		hardseal_close_with_delay( hardseal )
	end
end

function convert_label_number( number )
	if( number == 1 ) then
		return "1st"
	elseif( number == 2 ) then
		return "2nd"
	elseif( number == 3 ) then
		return "3rd"
	else
		return number .. "th"
	end
end	

--]]

-- SAMPLE PREPARATION --[[
function assert_sample_prep_global_constants()
	assert_boolean_value("PRE_HEAT_ELUTION", PRE_HEAT_ELUTION)
	assert_boolean_value("USE_HEATED_ELUTION", USE_HEATED_ELUTION)
	assert_boolean_value("BEAD_BEAT_SPLIT_MAGBEADS", BEAD_BEAT_MAGBEAD_HANDLING.BEAD_BEAT_SPLIT_MAGBEADS)
	assert_boolean_value("BEAD_BEAT_WITH_MAGBEADS", BEAD_BEAT_MAGBEAD_HANDLING.BEAD_BEAT_WITH_MAGBEADS)
	assert_boolean_value("BEAD_BEAT_AND_MIX_MAGBEADS", BEAD_BEAT_MAGBEAD_HANDLING.BEAD_BEAT_AND_MIX_MAGBEADS)
	assert_boolean_value("ELUTION.PRE_PLUNGE", PRE_PLUNGE_ELUTION.PRE_PLUNGE)
	assert_boolean_value("ELUTION.PLUNGED", PRE_PLUNGE_ELUTION.PLUNGED)
	assert_boolean_value("DILUTION.PRE_PLUNGE", PRE_PLUNGE_DILUTION.PRE_PLUNGE)
	assert_boolean_value("DILUTION.PLUNGED", PRE_PLUNGE_DILUTION.PLUNGED)
end

function is_valid_bead_beating_handling()
	local selected_count = 0
	for key,value in pairs(BEAD_BEAT_MAGBEAD_HANDLING) do
		if BEAD_BEAT_MAGBEAD_HANDLING[key] then
			selected_count = selected_count + 1
		end
	end
	return selected_count == 1
end

function bead_beat()
	if is_valid_bead_beating_handling() ~= true then
		create_error("One bead beat handling method must be chosen.")
	end
	if BEAD_BEAT_RPM < 4000 or BEAD_BEAT_RPM > 16000 then
		create_error( "Bead beat rpm " .. BEAD_BEAT_RPM .. " out of protocol's defined range of " .. 4000 .. " to " .. 16000 .. ".")
	end

	if BLISTERS.MAGBEADS ~= BLISTERS.LYSIS_BEADS and BEAD_BEAT_MAGBEAD_HANDLING.BEAD_BEAT_AND_MIX_MAGBEADS == false then
		move_liquid(FITMENT.BINDING_SOLUTION, BLISTERS.BEAD_MIX)
		mix_liquid(BLISTERS.BEAD_MIX, BLISTERS.MAGBEADS, 10)
		if BEAD_BEAT_MAGBEAD_HANDLING.BEAD_BEAT_WITH_MAGBEADS then
			move_liquid(BLISTERS.MAGBEADS, BLISTERS.LYSIS_BEADS)   
		elseif BEAD_BEAT_MAGBEAD_HANDLING.BEAD_BEAT_SPLIT_MAGBEADS then
			split_liquid(BLISTERS.MAGBEADS, BLISTERS.BEAD_MIX)
			bladder_vent( BLISTERS.BEAD_MIX )
			move_liquid(BLISTERS.BEAD_MIX, BLISTERS.LYSIS_BEADS)   
		end
	end

	plunge_and_bead_beat( BEAD_BEAT_RPM ) --handles bead beater rpm stalls, runs in different thread 
	local start_time = elapsed_time()
	local is_beating = true

	if BLISTERS.MAGBEADS ~= BLISTERS.LYSIS_BEADS and BEAD_BEAT_MAGBEAD_HANDLING.BEAD_BEAT_AND_MIX_MAGBEADS then
		move_liquid(FITMENT.BINDING_SOLUTION, BLISTERS.BEAD_MIX)
		for i = 1,10 do
			if  is_beating and (get_time_elapsed(start_time) > BEAD_BEAT_SECONDS) then
				bead_beater_off()
				is_beating = false
			else
				mix_liquid(BLISTERS.BEAD_MIX, BLISTERS.MAGBEADS, 1)
			end
		end
	end

	if is_beating then
		while get_time_elapsed(start_time) < BEAD_BEAT_SECONDS  do
			seconds_delay(1)
		end
		bead_beater_off()
		is_beating = false
	end

	if BLISTERS.MAGBEADS ~= BLISTERS.LYSIS_BEADS and BEAD_BEAT_MAGBEAD_HANDLING.BEAD_BEAT_WITH_MAGBEADS == false then
		mix_liquid(BLISTERS.BEAD_MIX, BLISTERS.MAGBEADS,2)  
		move_liquid(BLISTERS.MAGBEADS, BLISTERS.LYSIS_BEADS)  
	end
end

function plunge_and_bead_beat(rpm) --Firmware automatically checks for stalls
	move_liquid(FITMENT.SAMPLE, BLISTERS.LYSIS_BEADS)    
	if BEAD_BEAT_SECONDS > 0 then
		bead_beater_on(rpm)   
	end
	bladder_slow_fill_to_pressure(BLISTERS.LYSIS_BEADS,  SLOW_FILL_ACTUATION_TIME_MS, SLOW_FILL_PERIOD_MS, 4 )    --micropuff
end

function settle_lysis_beads( settle_seconds )
	local start_time = elapsed_time()
	local end_wait_seconds = 4
	local seconds_delay_adjustment = 3
	for i=1,5 do
		slow_fill_or_fill_bladder(BLISTERS.LYSIS_BEADS)		
		if i == 1 and PRE_PLUNGE_ELUTION.PRE_PLUNGE then
			move_liquid(FITMENT.ELUTION_BUFFER, BLISTERS.PCR_I_PELTIER1)
			PRE_PLUNGE_ELUTION.PLUNGED = true
		elseif i == 2 and PRE_PLUNGE_DILUTION.PRE_PLUNGE then
			plunge_piston(FITMENT.DILUTION_BUFFER)
			PRE_PLUNGE_DILUTION.PLUNGED = true
		elseif i == 3 and PRE_HEAT_ELUTION and USE_HEATED_ELUTION then
			ramp_to_temp( PELTIERS.PCR1, PRE_HEAT_ELUTION_TEMPERATURE, PRE_HEAT_ELUTION_RAMP_RATE)
		elseif i == 4 then
			end_wait_seconds = (settle_seconds - get_time_elapsed(start_time) - seconds_delay_adjustment )/2
			if end_wait_seconds < 0 then
				end_wait_seconds = 0
			end
			seconds_delay(end_wait_seconds)
		else
			seconds_delay(end_wait_seconds)
		end
		bladder_vent(BLISTERS.LYSIS_BEADS)
		seconds_delay(1)
	end
end

function collect_magbeads( collection_stage, capture_beads_table )
	if cycles == 0 then
		return
	end
	assert(collection_stage.SLOW_FILL_PRESSURES)
	local slow_fill_table = collection_stage.SLOW_FILL_PRESSURES
	local lysis_hardseal = get_hardseal(BLISTERS.LYSIS_BEADS, get_movement_iterator(BLISTERS.LYSIS_BEADS, BLISTERS.BEAD_MIX))
	if capture_beads_table.use_magnet == true then
		magnet_engage()
	end

	for i = 1, collection_stage.CYCLES do
		open_path(BLISTERS.MAGNET, BLISTERS.BEAD_MIX)
		bladder_vent( BLISTERS.MAGNET )
		slow_fill_or_fill_bladder(BLISTERS.LYSIS_BEADS, slow_fill_table)
		seconds_delay(0.3)

		hardseal_close_with_delay(lysis_hardseal)
		bladder_vent(BLISTERS.LYSIS_BEADS)

		capture_beads( capture_beads_table )
		hardseal_open(lysis_hardseal)
		close_path(BLISTERS.MAGNET, BLISTERS.BEAD_MIX, slow_fill_table)
		slow_fill_or_fill_bladder(BLISTERS.BEAD_MIX, slow_fill_table)
		seconds_delay( collection_stage.SETTLE_DELAY_SECONDS)
		if capture_beads_table.cycle_magnet then
			magnet_retract()
		end
	end
end

function capture_beads( capture_beads_table)
	assert_boolean_value("cycle_magnet", capture_beads_table.cycle_magnet)
	assert_boolean_value("use_magnet", capture_beads_table.use_magnet)

	if capture_beads_table.pre_magnet_delay_seconds > 0 then
		seconds_delay(capture_beads_table.pre_magnet_delay_seconds)	
	end
	if capture_beads_table.cycle_magnet then
		magnet_engage()
	end
	if capture_beads_table.post_magnet_delay_seconds > 0 then
		seconds_delay(capture_beads_table.post_magnet_delay_seconds)
	end
end   

function perform_elution( elution_mix_count, use_three_blister_elution, capture_beads_table, slow_fill_table )
	if PRE_PLUNGE_ELUTION.PLUNGED ~= true then
		move_liquid(FITMENT.ELUTION_BUFFER, BLISTERS.PCR_I_PELTIER1)
	end
	if USE_HEATED_ELUTION then
		slow_fill_or_fill_bladder( BLISTERS.PCR_I_PELTIER1 )
		ramp_to_temp( PELTIERS.PCR1, ELUTION_TEMPERATURE, ELUTION_RAMP_RATE)
		if PRE_PLUNGE_ELUTION.PLUNGED ~= true and PRE_HEAT_ELUTION ~= true then
			local warm_up_seconds = 5
			seconds_delay(warm_up_seconds)	
		end
	end
	move_liquid( BLISTERS.PCR_I_PELTIER1, BLISTERS.MAGNET )

	if use_three_blister_elution then
		mix_liquid( BLISTERS.MAGNET, BLISTERS.BEAD_MIX, elution_mix_count )
		close_path( BLISTERS.BEAD_MIX, BLISTERS.MAGNET )
	end

	mix_liquid( BLISTERS.MAGNET, BLISTERS.PCR_I_PELTIER1, elution_mix_count )

	if ELUTION_HOLD_SECONDS > 0 then
		close_path( BLISTERS.MAGNET, BLISTERS.PCR_I_PELTIER1 )
		slow_fill_or_fill_bladder( BLISTERS.PCR_I_PELTIER1 )
		seconds_delay( ELUTION_HOLD_SECONDS )
		open_path( BLISTERS.PCR_I_PELTIER1, BLISTERS.MAGNET )
		mix_liquid( BLISTERS.PCR_I_PELTIER1, BLISTERS.MAGNET, elution_mix_count )
	end
	
	slow_fill_or_fill_bladder( BLISTERS.PCR_I_PELTIER1 )
	capture_beads( capture_beads_table )
	bladder_vent( BLISTERS.PCR_I_PELTIER1 )
	open_path( BLISTERS.PCR_I_PELTIER1, BLISTERS.ELUTION, slow_fill_table )
	close_path( BLISTERS.MAGNET, BLISTERS.ELUTION, slow_fill_table )
	if capture_beads_table.use_magnet then
		magnet_retract()
	end
end
--]]

-- MASTER MIX --[[
function add_master_mix_II()
	open_path( BLISTERS.MASTER_MIX_II, BLISTERS.PCR_I_PELTIER1 )
	move_liquid( FITMENT.MASTER_MIX_II_B, BLISTERS.MASTER_MIX_II )
	seal_path( BLISTERS.MASTER_MIX_II, BLISTERS.PCR_I_PELTIER2 )
	mix_liquid( BLISTERS.PCR_I_PELTIER2, BLISTERS.PCR_I_PELTIER1, 6 )
	move_liquid( BLISTERS.MASTER_MIX_II, BLISTERS.PCR_I_PELTIER2 )
	mix_liquid( BLISTERS.PCR_I_PELTIER2, BLISTERS.PCR_I_PELTIER1, 6 )
end
--]]

-- DILUTION --[[
function flood_array()
	bladder_vent( BLISTERS.ARRAY ) --vent with override to update bladder status (mask find)
	open_path( BLISTERS.PCR_I_PELTIER2, BLISTERS.ARRAY )
    bladder_fill( BLISTERS.PCR_I_PELTIER1 )
	local additional_array_flood_seconds_delay = POST_FILL_DELAY_MS/1000
	seconds_delay(additional_array_flood_seconds_delay) 
	seal_path( BLISTERS.PCR_I_PELTIER1, BLISTERS.PCR_I_PELTIER2 )
	fill_path( BLISTERS.PCR_I_PELTIER2, BLISTERS.MASTER_MIX_II )
	seconds_delay( ARRAY_FLOOD_DELAY_SECONDS )
	open_path( BLISTERS.ARRAY, BLISTERS.MASTER_MIX_II )
	piston_retract()
	slow_fill_or_fill_bladder( BLISTERS.ARRAY )
end

function perform_PCR_I_dilution()    
   move_liquid( BLISTERS.PCR_I_PELTIER2, BLISTERS.WASTE ) 
   move_liquid( BLISTERS.PCR_I_PELTIER2, BLISTERS.WASTE ) --repeat for adequate PCR1 dilution 
   if PRE_PLUNGE_DILUTION.PLUNGED ~= true then 
      move_liquid( FITMENT.DILUTION_BUFFER, BLISTERS.PCR_I_PELTIER2 ) 
   else 
      move_liquid( BLISTERS.DILUTION, BLISTERS.PCR_I_PELTIER2 ) 
   end 
   mix_liquid( BLISTERS.PCR_I_PELTIER2, BLISTERS.PCR_I_PELTIER1, 6 ) 
   move_liquid( BLISTERS.PCR_I_PELTIER2, BLISTERS.WASTE ) 
   move_liquid( BLISTERS.PCR_I_PELTIER2, BLISTERS.WASTE ) --repeat for adequate PCR1 dilution 
end
--]]

-- PCR --[[
function assert_pcr_global_constants()
    assert_boolean_value("DILUTION.PRE_PLUNGE", PRE_PLUNGE_DILUTION.PRE_PLUNGE)
    assert_boolean_value("DILUTION.PLUNGED", PRE_PLUNGE_DILUTION.PLUNGED)
end

function perform_PCR_I_mix_sequence()
    bladder_vent(BLISTERS.PCR_I_PELTIER1)		
    local delay_in_seconds = POST_FILL_DELAY_MS / 1000
    seconds_delay(delay_in_seconds)
    bladder_vent(BLISTERS.PCR_I_PELTIER2)
    slow_fill_or_fill_bladder(BLISTERS.PCR_I_PELTIER1)
    slow_fill_or_fill_bladder(BLISTERS.PCR_I_PELTIER2)
end

function perform_PCR_I_stage(index)
    local initial_denature_hold = 4.0
    local stage = PCR_I_STAGES[index]

    for i = 1,stage.CYCLES do	
        ramp_to_temp_no_wait( PELTIERS.PCR1, stage.DENATURE_TEMPERATURE, stage.RAMP_RATES )		
        perform_PCR_I_mix_sequence()		
        wait_for_pcr1()
        seconds_delay( stage.DENATURE_HOLD_SECONDS )
        
        ramp_to_temp_no_wait( PELTIERS.PCR1, stage.ANNEALING_TEMPERATURE, stage.RAMP_RATES )		
        perform_PCR_I_mix_sequence()		
        wait_for_pcr1()
        seconds_delay( stage.ANNEALING_HOLD_SECONDS )
    end
end

function perform_PCR_II_stage( index )
    local stage = PCR_II_STAGES[index]
    for i=1,stage.CYCLES do 
        ramp_to_temp( PELTIERS.PCR2, stage.DENATURE_TEMPERATURE, stage.RAMP_RATES )
        seconds_delay( stage.DENATURE_HOLD_SECONDS )
        ramp_to_temp( PELTIERS.PCR2, stage.ANNEALING_TEMPERATURE, stage.RAMP_RATES )
        seconds_delay( stage.ANNEALING_HOLD_SECONDS )
        activate_excitation_led( )
        capture_fluor_temp_data_and_image("PCR II Stage " .. index .. " Cycle " .. i)
        deactivate_excitation_led()
    end
end

function perform_melt_from_pcr2_stage( index )
    local stage = PCR_II_STAGES[index]
    local melt = stage.MELT
    ramp_to_temp( PELTIERS.PCR2, melt.START_TEMPERATURE, PRE_MELT_RAMP_RATE )
    seconds_delay( PRE_MELT_HOLD_SECONDS )
    stop_reservoir_pressure_regulation()
    activate_excitation_led()
    capture_image( "Melt"..index.."_Acquisition_Beginning" )		
    start_collecting_protocol_data_periodically( FLUOR_TEMP_CAPTURE_QUERYABLE, 0.1 )
    ramp_to_temp( PELTIERS.PCR2, melt.RAMP_TEMPERATURE, melt.RAMP_RATES )		
    stop_collecting_protocol_data( FLUOR_TEMP_CAPTURE_QUERYABLE )
    capture_image( "Melt"..index.."_Acquisition_End" )
    deactivate_excitation_led()
    start_reservoir_pressure_regulation()
end
--]]

-- POUCH LAYOUT --[[
    FITMENT =	{
				SAMPLE =			{
											WELL =					1,
											CHANNEL_BLISTER =	"A",
											HARDSEAL =			"A_FITMENT"
										},
				WELL_2 =            { 
											WELL =               2, 
											CHANNEL_BLISTER =   "A", 
											HARDSEAL =         "A_FITMENT" 
										}, 
				BINDING_SOLUTION =	{
											WELL =					3,
											CHANNEL_BLISTER =	"B",
											HARDSEAL =			"B_FITMENT"
										},
				WASH_BUFFER_1 =		{
											WELL =					4,
											CHANNEL_BLISTER =	"B",
											HARDSEAL =			"B_FITMENT"
										},
				WASH_BUFFER_2 =		{
											WELL =					5,
											CHANNEL_BLISTER =	"B",
											HARDSEAL =			"B_FITMENT"
										},
				WASH_BUFFER_3 =		nil,
				ELUTION_BUFFER =		{
											WELL =					6,
											CHANNEL_BLISTER =	"D",
											HARDSEAL =			"D_FITMENT"
										},
				MASTER_MIX_I =		{
											WELL =					7,
											CHANNEL_BLISTER =	"D",
											HARDSEAL =			"D_FITMENT"
										},
				DILUTION_BUFFER =		{
											WELL =					8,
											CHANNEL_BLISTER =	"F",
											HARDSEAL =		"F_FITMENT"
										},
				MASTER_MIX_II_A =		{
											WELL =					9,
											CHANNEL_BLISTER =	"F",
											HARDSEAL =			"F_FITMENT"
										},
				MASTER_MIX_II_B =		{
											WELL =					10,
											CHANNEL_BLISTER =	"F",
											HARDSEAL =			"F_FITMENT"
										},
				OVERFLOW =			{
											WELL =					12,
											CHANNEL_BLISTER =	"G"
										}
    }
BLISTERS =	{
        LYSIS_BEADS =		"A",
        WASTE =			"A",
        BEAD_MIX =		"B",
        MAGBEADS =		"C",
        MAGNET =		"C",
        ELUTION =		"D",  
        PRIMER_PILL =		"E",
        PCR_I_PELTIER1 =	"D",
        PCR_I_PELTIER2 =	"E",
        DILUTION =		"F",
        MASTER_MIX_II =	        "F",
        ARRAY =			"G",
        PCR_II_PELTIER =	"G"  
}
BLADDERS =				    { "A", "B", "C", "D", "E", "F", "G" }
HARDSEALS =				    { "A_B", "B_C", "C_D", "D_E", "E_F", "F_G", "A_FITMENT", "B_FITMENT", "D_FITMENT", "F_FITMENT" }
SLOW_FILL_BLADDERS =		{"A", "B", "C"}
PELTIERS =					{ PCR1 = "PCR1", PCR2 = "PCR2" }
--]]

--RUN STEPS --[[
function run_protocol_step(name, func) 
    func()
end

function run_protocol()	
    local steps = {}
    for index,step in ipairs(STEPS) do
        table.insert(steps, {step.NAME, step.FUNCTION})
    end
    set_protocol_steps_queryable(steps)
    
    for index,step in ipairs(STEPS) do
        mark_protocol_step_started(step.NAME)
        run_protocol_step(step.NAME, step.FUNCTION)
    end
end

run_protocol()
--]]
